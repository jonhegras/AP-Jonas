let rotAngl = 0
let dotsX = 0

function preload() {
  bufferPic = loadImage('bufferPic.jpg');
}

function setup() {
  createCanvas (windowWidth, windowHeight);
  frameRate (10);
  angleMode(DEGREES);
  strokeWeight(2)
  noStroke();
}

function draw() {

      push();
    tint(255, 20);
    image(bufferPic, 0, 0, width, height);
      pop();
    translate(width/2, height/2);

      push();
    fill(255, 75, 75);
    ellipse(dotsX-15, 15, 10, 10)
    stroke(1);
    textSize(18);
    text('PLEASE WAIT', -60, 0)
      pop();

for (i = 1; i < 32; i = i * 2) {
      push();
    fill(200);
    rotate(rotAngl);
    scale(i);
    ellipse(100, 0, 50, 50);
      pop();
}
  rotAngl += 30
  dotsX += 15

  if (rotAngl > 359) {
    rotAngl = 0
  }
  if (dotsX > 30) {
    dotsX = 0
  }
}

function windowResized() {
  resizeCanvas(windowWidth, windowHeight);
}
