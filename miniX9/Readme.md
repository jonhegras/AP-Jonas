This program is made by [Jonas](https://gitlab.com/jonhegras), [Christian](https://gitlab.com/ChristianRohde) and [Mads](https://gitlab.com/madsdamkjaernielsen) from buddygroup 1. 
<br>

Link to the program
<br> https://jonhegras.gitlab.io/AP-Jonas/miniX9/

Link to the code
<br> https://gitlab.com/jonhegras/AP-Jonas/-/blob/master/miniX9/sketch.js


**What is the program about? Which API have you used and why?**
<br>
Our program is a segment of the atmosphere around the world, with this program we want to show how many satellites orbit the earth when the program is executed. 
We want it to be a quarter of the earth visible in the program and then we want the satellites to look as if rotating around the earth. The user should be able to use the mousePressed() function on the individual satellites and then the name of the specific satellite should appear in a science fiction-esque font. 
The API we used is from the National Aeronautics and Space Administration better known as NASA. They have a large database where we as programmers are allowed to use some of their databases. They call it the ‘NASA Open APIs’ which they describe as _“...this site is to make NASA data, including imagery, eminently accessible to application developers. The api.nasa.gov catalog is growing”_. (https://api.nasa.gov/).  
Our main goal for this project was to visualize some sort of live data, in a way that is interactive (to some degree). In terms of visualization, it was important for us to display the data that would feel ‘’real’’, so in this case we showcased the data from NASA satellites, by making a sketch that portrays the earth with orbiting satellites for each real satellite in space. The program is interactive, to the point, where a user is able to click on each satellite, and it will retrieve the name of the given object.
We want to mention that our main idea with the program in the start was to illustrate how many users that were online at the given time on Valve’s gaming platform steam. Steam is a big platform with a big library of games. The platform itself has millions of daily users online, and we thought it would be nice to see how many were online, and maybe filter it to show how many played some specific games. This was maybe a little bit ambitious for our group this week and we decided to go for another program and a little bit easier API. The problem with the Steam API was it was not as easily accessible and we needed some additional programs to illustrate it in the P5.js. 
<br>
<br>

**Can you describe and reflect on your process in this miniX in terms of acquiring,
processing, using, and representing data? How much do you understand this data or what do you want to know more about? How do platform providers sort the data and give you the requested data? What are the power relations in the chosen APIs? What is the significance of APIs in digital culture?**
<br>
As mentioned NASA does provide a range of data collections, stored in APIs, with this just being one of them. The data is structured in a JSON file with all the datasets (of satellites) combined in an array named ‘’member’’, as of writing this, there are currently 20 active satellites, ranging from the number 0-19 in the array. Each number (representing a satellite) contains a structured dataset, consisting of their name, date of last update from the position/state of the specific object, and two categories named ‘’line1’’ and ‘’line2’’. The last two categories contains a long list of numbers ex"1 44859U 19089H 21107.37147089 .00000163 00000-0 19875-4 0 9991", what each set of numbers represent are elaborated on this site: https://tle.ivanstanojevic.me/#/tle/44859. This amount of data feels quite ‘’rich’’, with the new tendency of corporations regulating and limiting the use of APIs to the private user. There is quite a lot of data hidden in the numbers, we do not quite understand, which could alienate some users from effectively using the API, but as the main goal for the project was to represent each satellite in orbit with the possibility to display the name of the satellites, we did not intend to understand every aspect of the data provided by NASA. However, if we were to rework the program in the future, it would be obvious to make more data visible, and potentially even try and translate some of the more difficult things to understand, so a less knowledgeable user inadvertently could gain more from the API. The NASA API service does have a limit of 1000 requests per hour, but with the possibility of contacting them, if more is needed. Whether or not this will be granted through a fee, we do not know, but nonetheless, it does limit the possible use of the data provided, however, for a small scale project like ours, it doesn’t make a difference, it still allows for us to create something, that is constantly acquiring a dataset to update the information in our sketch, without the need of human regulation (from our side). This potential and use of APIs make it possible to establish a program that feels alive to a degree that exceeds the potential of local stored data, with way less effort.
<br>
<br>

**Try to formulate a question in relation to web APIs or querying/parsing processes that you would like to investigate further if you had more time.**
<br>
One thing we had come up in our internal discussions was the question as to what data is actually being shared through the API’s we were searching through. A company might have 2 API’s, one for the public and one for very specific people, who might even consider paying a large amount of money for access. This adds to the issue of commercializing data, so it could be interesting to try and figure out what types of companies deal with what type of API’s, and subsequently what type of data they allow for whom.
<br>
![](ScreenMini9-0.jpg)
![](ScreenMini9-1.jpg)
<br>
**References:**
<br>
https://api.nasa.gov/
<br>
https://tle.ivanstanojevic.me/#/tle/44859
<br>
https://www.youtube.com/watch?v=6mT3r8Qn1VY&list=PLRqwX-V7Uu6a-SQiI4RtIwuOrLJGnel0r&index=4&ab_channel=TheCodingTrain
