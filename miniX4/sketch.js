let button; //defines button is a thing
let capture; //Webcam or video
let ctracker; //enables face tracker

let followVal = 0; //Changed when the button is pressed
let textFol = 'Current Followers:'; //text used
let folcount = 0; //amount of "followers"
let adsTrue = 0; //should the ads be on?

//declairing arrays
let folButton = [];
let addL = [];
let addR = [];

let l = []; //Randomly 0 or 1
let r = []; //Randomly 0 or 1 (redundant)

function preload() {
  //Loads images into arrays
  folButton[0] = loadImage('fol1.png');
  folButton[1] = loadImage('fol2.png');

  addL[0] = loadImage('add1.jpg')
  addL[1] = loadImage('add2.jpg')

  addR[0] = loadImage('add3.jpg')
  addR[1] = loadImage('add4.jpg')

}

function setup() {
  createCanvas (640, 480);
  frameRate (60);
  background (0, 100, 255)

l = floor(random(2)); //sets the number randomly to 0 or 1
r = floor(random(2)); //sets the number randomly to 0 or 1 (redundant)

  //"Loading..." text for before the camera is initialized
  push();
    textSize(50);
    text('Loading...', width/2-90, height/2);
  pop();

  // Creates an interactable button
  button = createImg('fol1.png');
  button.size(118.5, 60);
  button.position(width/2-59, height - 70);
  button.mousePressed(follow);

  // creates a secondary button
  button2 = createImg('fol2.png')
  button2.size(118.5, 60);
  button2.position(width/2-59, height - 70);
  button2.mousePressed(follow);
  button2.hide();

  // enables video to be used through a web cam
  capture = createCapture(VIDEO);
  capture.size(640,480); //defines capture size
  capture.hide(); //hides the video capture

  //enables the face tracker
  ctracker = new clm.tracker();
  ctracker.init(pModel); //initialises the face capture
  ctracker.start(capture.elt);

}

function draw() {
  //print(followVal)

  image(capture, 0,0, 640, 480); //displays the webcam 1 frame at a time, making it look like a live feed

  let posT = ctracker.getCurrentPosition(); //defines tracker points on face

  if (followVal === 1){
   folcount = folcount + int(random(-1,5)); //increases number of followers by a random interval

 }

  // for loop that allows for continuos placement of the text and numbers on face.
  // - positions variable[facial point][x coordinate], positions variable[facial point], [y coordinate] -
  for(let i = 0; i < posT.length; i++){
    stroke(1);
    textSize(15);
    fill(255);
    text(textFol, posT[33][0]-50, posT[33][1]-60 - folcount / 300);
    push();
      stroke(1);
      strokeWeight(3);
      textSize(15 + folcount / 200);
      text(folcount, posT[33][0]-30, posT[33][1]-40);
    pop();
  }
  // for loop that allows for continuos placement of black bar over eyes if a condition is met.
  if (followVal === 0){
    for(let i = 0; i < posT.length; i++){
      push();
        strokeCap(SQUARE);
        strokeWeight(70);
        line(posT[27][0]-70, posT[27][1], posT[32][0]+70, posT[32][1], 100);
      pop();

    }
  }
  //determines whether the ads should appear
  if (adsTrue === 1){
  push();
    tint(255, 200);
    image(addL[l], 0, 0, 130, 480);
    image(addR[r], width-130, 0, 130, 480);
  pop();
}

}
//funciton that is called when the button is pressed. Controls the "on/off" conditions
function follow() {
if (followVal === 0) {
  followVal = 1;
  adsTrue = 1
  button.hide();
  button2.show();
} else {
  followVal = 0
  button2.hide();
  button.show();
  }
}
