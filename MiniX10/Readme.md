**Draw a flowchart to represent the program**

[See MiniX7 Project used for flowchart here](https://jonhegras.gitlab.io/AP-Jonas/miniX7/) <----

![](Narwhal_Game_Flowchart.png)


**What are the difficulties involved in trying to keep things simple at the communications level whilst maintaining complexity at the algorithmic procedural level?**
Computational language isn’t always easily translated, which can lead to certain nuances getting lost when trying to understand the underlying implication of a piece of software. Having to condense how something work into layman terms can lead to specific points either being misinterpreted or completely missed. This problem exists when a product or software has already been made, but take a scenario where a piece of software has to be pitched to someone who doesn’t understand the underlying technicalities: having to change the way you explain something, be it with a flowchart, an elevator pitch or something else, in a way you aren’t used to, can lead to misunderstandings regarding what the software is trying to say or be. However, you could also argue that this could expand your point of view and consider things you wouldn’t have otherwise.

**What are the technical challenges facing the two ideas and how are you going to address these?**
We have a lot of plans involving various subjects and syntaxes of which we’ve used throughout the semester, and one of the main challenges is trying to get all of it to work together. We are definitely challenging ourselves with some of the ideas, but they don’t seem impossible, so with some help from each other, whom all tend to have expertise and strength in various areas, and some help from our good friend Daniel Schiffman, we should be able to overcome any technical problems we might have. If something doesn’t work out like we thought it would, then there is likely an alternative we can try to explore instead.

**In which ways are the individual and the group flowcharts you produced useful?**
In essence, it was a really good tool for brainstorming. It allowed us to visualize our ideas, without having to actually make some sort of prototype. It also give us a good idea of what the structure of the code should look like, which gives us the ability to plan how we want to tackle potential, difficult technical aspects as well as how we best can allocate our work force; who does what and when.

**Idea Number 1**

![](MiniXide1.jpg)


**Idea Number 2**

![](MiniXide2.jpg)
