let snake;
let gridScl = 20
let food;

let rotAngl = 0
let dotsX = 0

function setup() {
  createCanvas (600, 600);
  frameRate (10);
  angleMode(DEGREES);
  snake = new Snake();
  //food = new Food();
  pickLoc();
}

function pickLoc() {
  let cols = floor(width/gridScl);
  let rows = floor(height/gridScl);
  food = createVector(floor(random(cols)), floor(random(rows)))
  food.mult(gridScl);
}

function draw() {
  background(50,100);
push();
  translate(width/2, height/2)
  push();
fill(255, 75, 75);
ellipse(dotsX-15, 15, 10, 10)
stroke(1);
textSize(17);
text('PLAY A GAME', -55, -20)
text('WHILE YOU WAIT', -70, 0)
text('points ' + snake.total,210, -260)
  pop();

for (i = 1; i < 32; i = i * 2) {
  push();
fill(200);
rotate(rotAngl);
ellipse(100, 0, 50, 50);
  pop();
}
rotAngl += 30
dotsX += 15

if (rotAngl > 359) {
rotAngl = 0
}
if (dotsX > 30) {
dotsX = 0
}
pop();


  snake.fail();
  snake.move();
  snake.show();
  fill(255,70,30);
  ellipse(food.x +10, food.y +10, gridScl, gridScl)

  if (snake.eat(food)) {
    pickLoc();
  }

}

function keyPressed() {
  if (keyCode === UP_ARROW) {
    snake.dir(0, -1);
  } else if (keyCode === DOWN_ARROW) {
    snake.dir(0, 1);
  } else if (keyCode === RIGHT_ARROW) {
    snake.dir(1, 0);
  } else if (keyCode === LEFT_ARROW) {
    snake.dir(-1, 0);
  }
}

function Snake() {
  this.x = 10;
  this.y = 10;
  this.xspeed = 1;
  this.yspeed = 0;
  this.total = 0;
  this.tail = [];

  this.eat = function(pos) {
    let d = dist(this.x-10, this.y-10, pos.x, pos.y);
    if (d < 1) {
      this.total++;
      return true;
    } else {
      return false;
    }
  }

  this.dir = function(x, y) {
    this.xspeed = x;
    this.yspeed = y;
  }

  this.fail = function() {
    for (var i = 0; i < this.tail.length; i++){
      let pos = this.tail[i]
      let d = dist (this.x, this.y, pos.x, pos.y);
      if (d < 1) {
        this.total = 0;
        this.tail = []
        this.x = 30
        this.y = 30
        pickLoc();
      }
    }

  }

  this.move = function() {

    if (this.total === this.tail.length) {
      for (let i = 0; i < this.tail.length-1; i++) {
        this.tail[i] = this.tail[i+1];
      }
    }
    this.tail[this.total-1] = createVector(this.x, this.y)

    this.x = this.x + this.xspeed*gridScl;
    this.y = this.y + this.yspeed*gridScl;

    this.x = constrain(this.x, 10, width-gridScl+10);
    this.y = constrain(this.y, 10, height-gridScl+10);
  }


  this.show = function() {
    fill(255);
    noStroke();
    for (let i = 0; i < this.tail.length; i++) {
      ellipse(this.tail[i].x, this.tail[i].y, gridScl, gridScl);
    }
    ellipse(this.x, this.y, gridScl, gridScl);

  }
}
