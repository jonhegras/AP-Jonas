[See project here](https://jonhegras.gitlab.io/AP-Jonas/miniX6/) <-----

[Code](https://gitlab.com/jonhegras/AP-Jonas/-/blob/master/miniX6/sketch.js) <-----


## What
I decided to rework my throbber MiniX, because it is arguably the one I put the least amount of time into. I felt like I could do more, and since I have an interest in game design, I wanted to try and incorporate some sort of interactive game, something with a point system.
The last time I wrote the code, I wanted to explore the annoyance and frustration one feels when dealing with endless throbbers, but there is definitely more to be said, and Aesthetic Programming is a tool which can be used to explore more than the practical aspects of software. Like with capitalistic surveillance, the actual intentions of data collection are disguised to help make the user not realize what is being taken, so I wanted to try and implement this with the throbber, and try to distract the user from realizing that the big throbber in the middle actually has no purpose and is simply not loading or buffering anything, by distracting them with a simple game of snake, effectively displaying that something that seems harmless, is in actuality there to distract from something the people behind want to hide. There is always a bias.
## Why
Programming is a vital part in digital culture. Without it, we would be walking around with bricks in our pockets instead of small portals that show us the world at an instance. But software and programming is not just for practical use, which is why aesthetic programming is so important, as it enables us to discuss and explore the humanitarian parts of digital culture and be critical about parts we wouldn’t be able to discuss if it was all simply practical. It’s a way to help us improve as a society, and as humans; help us strive towards proper post-humanism.
## How
I removed the screenshot that was meant to be the background and instead of having the screen fit according to window size, I set it to something specific. Then, instead of having the throbber multiply and increase in size, I simply had it in the middle as one, and then I changed the “please wait” red text in the middle to “play a game while you wait”. The next thing was that I had to get a game of snake to actually play. This took way more effort than expected, but I made it work in the end with a little help from a The Coding Train video ( https://www.youtube.com/watch?v=AaGK-fj-BAM ). This was great, because it taught me how to use function objects, which will certainly come in handy in the future.

![](ScreenMini6.jpg)

