[See project here](https://jonhegras.gitlab.io/AP-Jonas/miniX5/) <-----

[Code](https://gitlab.com/jonhegras/AP-Jonas/-/blob/master/miniX5/sketch.js) <-----

## Rules
I was quite inspired by 10 PRINT (Montfort et al.) and wanted to embrace the idea of very little and something very simple achieved quite a lot. So I started thinking of rules, and came up with 2 simple ones. I would have a shape of some sort of start moving from some place in the top left corner and then whenever it hit the right wall, it would start moving left and vice versa. Then, in a similar fashion, when it hits the bottom, it would start going up, and down when hitting the top. I then managed to use for loops to have the square repeatedly change from white to black. This would over time create some interesting shapes or patterns, similar to what 10 PRINT creates. Even when the screen fills, it continuously changes and mixes the pattern.
Now, to ensure the pattern or shape didn’t remain the same, I had the canvas size be randomly selected within specific boundaries every time the program starts. Since the randomness in this program comes from when the square hits the edges, this is important. This forces the pattern, and thereby the computer, to create something different each time. Thereby, the random canvas size enables for more random movements.

## Thoughts
An interesting thing to consider is whether the computer or the programmer gets the privilege of calling themselves the author of art pieces like this. There is 0 input from the programmer’s side, once the program is executed, thereby it could be argued, that the computer is actually the artist. On the other hand, the program that creates the art piece was made by someone, and the rules set in place were made by the author and programmer. Considering that, that would make the programmer the artist. But would that make the creator of the ball point pen the artist of every drawing made with a ball point pen? The ink cannot be erased, it only has the colors produced available. The creator of the pen made the rules, and the user of the pen draw according to those rules. In reality, the drawing made with a pen wouldn’t be possible without both parts: the creator of the pen and the person drawing with said pen. Same could be argued when it comes to auto generated art. The programmer has little to no control over what exactly is drawn, this is the program’s job. Co-design needs to happen between human and computer to create proper auto-generative art. The program isn’t necessarily following orders, it is more that they are given directions, and then try to create something according to that, similar to how leaders and employees work together in an office, or director and actor in a movie.

![](ScreenMini5.jpg)
![](ScreenMini5-2.jpg)
![](ScreenMini5-3.jpg)
![](ScreenMini5-4.jpg)
