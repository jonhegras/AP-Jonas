// askData start _______________________________________________________________
function askData(){
  //shows the question from the dataQuestions JSON-file

push();
  fill(255);
  textSize(32);
  text(dataQuestions.ask[n],50,560);
pop();

  enter.show();
  cancel.show();
  input.show();
}
//askData end __________________________________________________________________



//storeData start ______________________________________________________________
function storeData(){
  //stores the users input in an array

  if(input.value()===""){
    //constrain so that input cannot be empty
    please.style('font-size','20px');
    please.style('color','red');
    please.position(655,555);
    please.show();
  } else {
    please.hide();
    data.push(input.value());
    input.value(""); //reset input bar

  // this is just to check that name and values match (ignore)
    print(dataQuestions.ask[n]+":"+data[n]);
    print(n);
    n++;


    //resets timers and sets mood to satisfied
    resetTimers();
    startAskData = false;
    amountFeed++
    fed = true;
    mood = 2;
    enter.hide();
    cancel.hide();
    input.hide();

    }
}
// storeData end _______________________________________________________________

function displayData(){

  seeData.hide();
  push();
  fill(0);
  rect(100,50,900,500);
  translate(90,-115);

fill(0,255,0);
textSize(32);
textFont('Helvetica');
text("description: "+json.description,20,200);
text("name: "+json.name,20,225);
text("visited: "+json.visited,20,250);
text("play time: "+json.visitTime,20,275);
text("times fed: "+json.timesFeed,20,300);
text("times cleaned: "+json.timesClean,20,325);
text("times pet: "+json.timesPet,20,350);
text("favorite color: "+ json.favoriteColor,20,375);
text("nationality: "+json.nationality,20,400);
text("data of birth: "+json.dateOfBirth,20,425);
text("email: "+json.email,20,450);
text("phone number: "+json.phoneNum,20,475);
text("snapchat account: "+json.snapchat,20,500);
text("instagram account: "+json.instagram,20,525)
text("occupation: "+json.occupation,20,550);
text("address: "+json.address,20,575);
text("passport: "+json.passportNum,20,600);
text("social security number: "+json.socialSecurityNum,20,625);
text("bank details: "+json.bankDetails,20,650);
textSize(12);
fill('red');
text("DISCLAIMER: only you can see this data, none has been stored anywhere",-70,795);
pop();


}


// createJSON start_____________________________________________________________
function createJSON(){ //only executed once

  //creates JSON-file with name and value
      //we were not able to to use saveJSON for some reason :(

  /*
  Note: The data is abstracted both in createJSON() and displayData.
  This is because createJSON creates a snapshot of the data stored when
  the seeData-button was pressed. The displayData-function is continously
  drawn so the data is drawn as text on top layer of the canvas.

  If both the capturing of the data and the showing of the data was contained
  by the same function, some data like date of visit, would not be static, but
  would keep counting. This is to imitate that the data is stored as a static
  file.


  */

    json = {
    description: "Data Profile",
    name: data[0],
    visited: day()+"/"+month()+"/"+year()+" "+hour()+":"+minute()+":"+second(),
    visitTime: int(frameCount/60)+" sec",
    timesFeed:amountFeed,
    timesClean:amountClean,
    timesPet:amountPet,
    favoriteColor: data[1],
    nationality:  data[2],
    dateOfBirth: data[3],
    email: data[4],
    phoneNum: data[5],
    snapchat: data[6],
    instagram: data[7],
    occupation: data[8],
    address: data[9],
    passportNum: data[10],
    socialSecurityNum: data[11],
    bankDetails: data[12]
  }
  console.log(json);

    dataRights = true;


}
// createJSON end_______________________________________________________________
