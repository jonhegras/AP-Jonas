[See project here](https://jonhegras.gitlab.io/AP-Jonas/miniX1/) <-----

[Repository](https://gitlab.com/jonhegras/AP-Jonas/-/blob/master/miniX1/sketch.js)

What have I produced? I guess that is up to interpretation. I like to think of it as interactable clown vomit. More specifically; I have created a canvas where randomly placed circles appear and the width and height is equal to where on the screen the user places their mouse cursor in real time, allowing for some creativity from the user's side. After 200 frames it then switches to placing random squares instead in a different color. And then after another 200 frames, it resets and places circles again.
The colors on both shapes are semi-random, or at least has random elements, to make it more fun to look at. Although both shapes have a defined color base. Circles are red and squares are blue. The tint of red and blue are randomly chosen.
I was able to make it change from circles to squares by using a simple "if-else" sequence, where it uses frames elapsed as a variable and then counts the amount of frames that have passed, and depending on how many, decides whether to spawn blue squares or red circles. I also had to change the framerate from 60 to 10 frames per second, to slow it down, just to make it easier and more pleasant to look at.
Using the framecount to determine the shape, might not be the most optimal way of doing this, but since it is an already variable number, tied to time, it saved me from having to set up my own variable.

This isn't my first time using either Javascript or the P5 library, but I have definitely forgotten a lot of things, as last time I used it was maybe 3-4 years ago. So it has been nice using this to relearn some basics. it never got too complicated back then, but we did make some small games and what we would call coding art. Having a basic understanding of how coding works, definitely helps getting med back into things. So my first independant coding experience, in this class, has been almost rejuvinating, and I feel excited to get back into coding and learn more.

Coding isn't incredibly different from reading a book. You read from top to bottom, from left to right, and so does the computer when reading the code.
As someone who enjoys learning new languages, it fits well here as well. Both spoken language and coding language has specific dictionaries and phrases, which mean different things. Although in coding, there isn't really an equal to stuff like sarcasm and the likes, as the computer will interpret everything written literally. So you have to be very specific, compared to normal writing or speak, where you can imply things, and still be understood. "Robotic speech" is a phrase for a reason.

Computers and technology are a big part of my life. Both when it comes to hobbies, but also with work. Programming is the backbone of a lot of things I enjoy, and generally the backbone of society at the moment. Ever since I had "IT" as an extra subject during 3rd year of gymnasium, I have been an advocate of teaching at least the basics of programming and how software and hardware functions to younger generations. Anette Vee writes about this as well, and gives reason to the argument. Literacy leads to power, and those who have the ability will hold the power. Therefore it is important that we as a society equally allow everyone to gain this literacy. We saw it with reading and writing, and now history is in a sense repeasting itself with programming.

![](ScreenMini1.jpg)

Annette Vee; "Coding for Everyone and te Legacy of Mass Literacy" 43-93
