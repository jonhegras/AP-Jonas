class Player{
  constructor(_x, _y) {
  this.x = _x;
  this.y = _y;
  this.xSpeed = 0
  this.ySpeed = 0
}

  show() {
    fill(255);
    image(playerSprite, this.x, this.y)
    }

  dir(_x, _y) {
    this.xSpeed = _x;
    this.ySpeed = _y;
  }

  move() {
    this.x = this.x + this.xSpeed
    this.y = this.y + this.ySpeed
    this.x = constrain(this.x, 11, width - 11);
  }


}

  class Obstacle {
    constructor(_x, _y, _w, _h) {
    this.x = _x;
    this.y = _y;
    this.w = _w;
    this.h = _h;
    this.ySpeed = 10
}
  show() {
    image(obsBergSprite, this.x, this.y, this. w, this.h)
  }

  move() {
    this.y = this.y + this.ySpeed + obsSpeed
  }

  hit(pos) {
    let d = dist(this.x, this.y, pos.x, pos.y);
    if (d < 60) {
      return true;
    } else {
      return false;
    }
  }

}
