let player;
let bergs = [];
let createBerg;
let obsLoc;
let obsSpeed = 0
let score = 0
let backMove = -600

let start = 0

function preload (){ //loads images for later use
  playerSprite = loadImage('playerSprite.png');
  obsBergSprite = loadImage('obsBerg100x80.png');
  backdrop = loadImage('backdrop.png');
}

function setup() {
rectMode(CENTER);
imageMode(CENTER);
player = new Player(200, 560) //creates the player from the "Player" class
obsLoc = random(55, 345) //makes sure the icebergs apear randomly on the x axis
createBerg = new Obstacle(obsLoc, 0, 100, 80) //creates the icebergs from the "Obstacle" class
  for (let i = 0; i < 1; i++) {
    bergs[i] = createBerg //shifts through array to make obstacles
  }

  createCanvas(400, 600);

}


function draw() {
  push();
    imageMode(CORNER);
    image(backdrop, 0, backMove); //creates background
  pop();
  player.show();
  player.move();
  obsLoc = random(55, 345)


//text and slight movement before game start. Controlled by "start" variable.
if (start === 0) {
  push();
    text('PRESS LEFT OR RIGHT ARROW TO MOVE', 80, height/2 -20)
    text('YOU ->', 150, 565)
  pop();
  if (player.x === 200) {
    player.dir(0.1, 0);
  } else if (player.x > 208) {
    player.dir(-0.1, 0);
  }
}


if (start === 1) { //checks wether the player has started the game or not

backMove+=5 //moves background

if(backMove > 0) {
  backMove = -600 //resets background so it can keep looping
}


  for (let i = 0; i < bergs.length; i++) {
    bergs[i].show();
    bergs[i].move();
    if (bergs[i].y === 300){
      bergs.push(new Obstacle(obsLoc, 0, 100, 80)); //creates more obstacles
    } else if (bergs[i].y > 601) {
      bergs.splice(0, 1); //removes obstacle from screen
      score++ //counts score when an obstacle passes
    }

    if (bergs[i].hit(player)) { //lose condition
      bergs.splice(1, 1);
    push();
      strokeWeight(1)
      stroke(35);
      textSize(50);
      text('GAME OVER', 45, 120);
      textSize(20);
      text('SCORE '+score, 70, 170);
      noLoop();
    pop();
    }

  }
}


//sends the player in the opposite direction, if they hit the wall
if (player.x === 11) {
  player.dir(4, 0);
} else if (player.x === width - 11) {
  player.dir(-4, 0);
}

}
