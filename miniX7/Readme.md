[See project here](https://jonhegras.gitlab.io/AP-Jonas/miniX7/) <-----

[sketch.js](https://gitlab.com/jonhegras/AP-Jonas/-/blob/master/miniX7/sketch.js) <-----

[objects.js](https://gitlab.com/jonhegras/AP-Jonas/-/blob/master/miniX7/objects.js) <-----

[controls.js](https://gitlab.com/jonhegras/AP-Jonas/-/blob/master/miniX7/controls.js) <-----



## What Have I Made
I’ve created a simple score-based game, where you control a narwhal that has to navigate through oncoming icebergs. The more icebergs you dodge, the more points you score.
The game is separated into 3 states: not started, game in progress and game over.
This is done with a variable that keeps track of whether or not an arrow key has been pressed for the first time. Before the game starts, the player is met with a simple instruction, telling them to press an arrow key to move, prompting them to press one. You see the narwhal, which is the player character, swaying from side to side. Once the player presses a button, the game begins, and icebergs start spawning and moving towards the player, and the player is able to move to either side, to try and dodge them. A counter keeps track of the score, and is shown when the game is over. The game over state happens when the player hits an iceberg.
## The Classes as Objects
I’ve created 2 classes: one class to represent the player character, which in this case takes form as a narwhal, and one class which overall is the obstacles, which then gains the appearance of icebergs.
Practically, the use of classes allows for interaction and to keep “spawning” more of the same class. Using an array, I am able to keep spawning new icebergs, so the game may continue until the player class and any of the obstacle classes intersect, causing a conditional statement to freeze the screen and cause a “game over”. Over all, classes are incredibly useful for creating interactions between different things on screen.
The obstacle could in theory be anything, even just a square, and the game would work just the same. The class mainly gives the obstacle “personality” in the sense of how it behaves. In this case, since it is an iceberg, it is implied that the narwhal is killed upon collision, but if the obstacle visually was i.e., a marshmallow, then the implication would be completely different. Even if the game still end when you hit the theoretical marshmallow, then you wouldn’t assume that the game was over because the narwhal was killed, but rather that maybe it now had too much to eat, and had to stop due to a bellyache. The obstacle class gains specific attributes that make the game work at a basic level, but the attributes is also what enables us to interpret what is happening at a deeper level. It gives pixels and code character, and makes the classes feel “real”. 
## Classes and Culture
With classes, the program can do specific things, depending on what attributes are applied to the different classes. You could for example have a race between 2 objects i.e. a square and a circle. Then have them randomly be given a color, like red and blue, or if you wanted to be right on the nose: black and white. Then have one of the colors come with a faster movement speed, which would mean that one color would be “better” than the other, always giving it an advantage. Of course, this would be, like mentioned, a very on the nose statement about race inequality, but this was just a quick idea on how classes can help say something about society in a cultural context.

![](ScreenMini7.jpg)

![](ScreenMini7-1.jpg)

![](ScreenMini7-2.jpg)

