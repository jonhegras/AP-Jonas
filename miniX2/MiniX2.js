//create Arrays
let lEye = [];
let rEye = [];
let mouth = [];

function preload() { //load images from same folder
//left Eye load
  lEye[0] = loadImage('LEye1.png');
  lEye[1] = loadImage('LEye2.png');
  lEye[2] = loadImage('LEye3.png');
  lEye[3] = loadImage('LEye4.png');
  lEye[4] = loadImage('LEye5.png');
  lEye[5] = loadImage('LEye6.png');
//right Eye load
  rEye[0] = loadImage('REye1.png');
  rEye[1] = loadImage('REye2.png');
  rEye[2] = loadImage('REye3.png');
  rEye[3] = loadImage('REye4.png');
  rEye[4] = loadImage('REye5.png');
  rEye[5] = loadImage('REye6.png');
//mouth load
  mouth[0] = loadImage('Mouth1.png');
  mouth[1] = loadImage('Mouth2.png');
  mouth[2] = loadImage('Mouth3.png');
  mouth[3] = loadImage('Mouth4.png');
  mouth[4] = loadImage('Mouth5.png');
  mouth[5] = loadImage('Mouth6.png');
}


function setup() {
  createCanvas (890, 830);
  frameRate (7);
  strokeWeight(4);
  background(40, 40, 80);
  textSize(100);
  fill(100, 100, 0);
  text('Press Any Key', 110, 300);
  noFill();
  push();
    strokeWeight(20);
    rect(0, 0, 890, 830);
  pop();
  createMeh(); //calls the funtion I made to draw the "meh" emoji
  createHap(); //calls the funtion I made to draw the "happy" emoji
}


function draw() {
  if(keyIsPressed) { //draw the real faces
    let size = 400 + random(+ 50, -50);
    for (let i = 0; i < 2; i++) {
      background(40, 40, 80, 255);
      push();
      strokeWeight(20);
      rect(0, 0, 890, 830);
      pop();
      createMeh();
      createHap();
      image(random(lEye), width/10, height/10, size, size);
      image(random(rEye), width/10 + 300, height/10, size, size);
      image(random(mouth), width/10 + 150, height/10 + 250, size, size);
      }
    }
  }


function createMeh() { //custom function to create "meh" emoji
  //Draw Meh Emote
  for (let mehY = 40; mehY < height; mehY += 100) {
    for (let mehX = 55; mehX < width; mehX += 120) {
      fill(random(200, 255), random(200, 227), 0);
      ellipse(mehX, mehY, 40, 40);
      line(mehX+10, mehY+10+random(+3, -3), mehX-10, mehY+10+random(+3, -3));
      point(mehX+5+random(+1, -1), mehY-4);
      point(mehX-7+random(+1, -1), mehY-4);
      }
    }
  }


function createHap() { //custom function to create "happy" emoji
  //Draw Happy Emote
  for (let hapY = 90; hapY < height; hapY += 100) {
    for (let hapX = 115; hapX < width; hapX += 120) {
      fill(random(100, 255));
      ellipse(hapX, hapY, 40, 40);
      noFill();
      beginShape();
        vertex(hapX-10, hapY+5+random(+1, -1));
        vertex(hapX, hapY+13+random(+3, -3));
        vertex(hapX+10, hapY+5+random(+1, -1));
      endShape();
      point(hapX+5+random(+1, -1), hapY-4);
      point(hapX-8+random(+1,-1), hapY-4);
      }
    }
  }
