[VIEW PROJECT   ](https://jonhegras.gitlab.io/AP-Jonas/miniX8/) | [   VIEW CODE](https://gitlab.com/jonhegras/AP-Jonas/-/blob/master/miniX8/sketch.js) <br>

### Let us introduce you to J00-lean

The main idea arose while one of us was walking in the woods and thought how can you use both a JSON.file to store data and also make it literary. 
The program is partly inspired by a lie detector in which the subject is asked yes or no questions. We tried to emulate that rigid structure in our program. 
We thought it would be interesting if we could ask the user questions made by humans, but the human needs to use Booleans to answer True/False. By doing this, in some way, we set up a dialog between a human and the computer but with opposite roles, the computer asks human-ish questions and the user has to answer with a boolean expression which is a way for a computer to answer with code language.

By asking questions where true/false does not suffice, we are exposing or highlighting the challenge/absurdity in trying to compress complexity in the “perfect match” as described by Sadie Plant in Soon Winnie & Cox, Geoff, Aesthetic Programming: A Handbook of Software Studies.

> “The zeros and ones of machine code seem to offer themselves as perfect symbols of the orders of Western reality (...) white and black, good and evil, right and wrong (...) penis and vagina, thing and hole (…) hand in glove. A perfect match.” - Soon & Cox s. 183

Sometimes the given options don't make sense grammatically but at all times the user is left feeling restricted by the limitations set by our program. There is no room for elaboration or ways to explain the complexity of intentions or thoughts. 
All answers are forced in the narrow scope of either true or false. No in-between. 

While further developing our program we thought of the J3RR1 machine which led us to start thinking of and creating our program as its own being and made us come up with the concept of having our program in a sense develop itself.

The computer starts out by asking the user questions looking outside trying to learn more about the world. The questions topic appears random and phrased slightly awkward given the answer options. As the program goes on, the program/computer starts asking questions about its own existence in this world and looking more inwards. The otherwise bit-like static interface is being disrupted by organic shapes of green vines contrasting the grey to symbolize life. By the end of the questions the program finishes off by displaying an end message thanking the user for their help and signing off with a signature giving the impression that the program is slightly more sentient. 

<img src ='ScreenMini8.jpg' width=400> <img src ='ScreenMini8-1.jpg' width=400> <br>
<img src ='ScreenMini8-2.jpg' width=400> <img src ='ScreenMini8-3.jpg' width=400>

##### JSON
We wanted to store all our questions in the JSON.file
We thought it would be very nice if we could put all our premade questions into the JSON.file. With the JSON.file it is possible for us to store a very large load of data, but contained in a manner that makes it easier for us to work with.
Using a JSON file, we were able to store a ton of questions in 2 arrays, enabling us to load specific types of questions at certain points. The first batch of questions are rather random, and will be displayed as the first 20 questions. After 20 questions have been answered, then the program begins to pick questions from the second array in our JSON file, containing questions that sound like the program or computer itself is asking them.
The program counts the amount of times you click either button with a variable. This variable also allows us to control the amount of fade that is applied to the vines, making them become more and more visible in correlation with the amount of answers given.
 

##### References: 

- Soon Winnie & Cox, Geoff, Aesthetic Programming: A Handbook of Software Studies, London: Open Humanities Press, 2020.
- Link til J3RR1: [https://nonecollective.it/artworks/j3rr1/](https://nonecollective.it/artworks/j3rr1/)

